using System.ComponentModel.DataAnnotations;

namespace MazzaFC_API.Models
{
    public class Categoria
    {
        [Key]
        public int CdCategoria { get; set; }

        [Required(ErrorMessage = "Este campo � obrigat�rio.")]
        [MinLength(3,ErrorMessage = "Este campo deve ter mais de 3 caracteres")]
        [MaxLength(80, ErrorMessage = "Este campo deve ter at� 80 caracteres")]
        public string Descricao { get; set; }

    }
}