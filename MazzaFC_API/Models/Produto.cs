using System.ComponentModel.DataAnnotations;

namespace MazzaFC_API.Models
{
    public class Produto
    {
        [Key]
        public int ID { get; set; }
        
        [Required(ErrorMessage = "Este campo � obrigat�rio.")]
        [MinLength(5, ErrorMessage = "Este campo deve ter mais de 5 caracteres")]
        [MaxLength(100, ErrorMessage = "Este campo deve ter at� 100 caracteres")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Este campo � obrig�t�rio.")]
        public int CdCategoria { get; set; }

        [Required(ErrorMessage = "Este campo � obrigat�rio.")]
        [Range(1,int.MaxValue,ErrorMessage = "O valor do produto deve ser maior que zero.")]
       
        public decimal preco { get; set; }
    }
}