using System.ComponentModel.DataAnnotations;

namespace MazzaFC_API.Models
{
    public class Usuario
    {   
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Este campo � obrigat�rio.")]
        [MinLength(5, ErrorMessage = "Este campo deve ter mais de 5 caracteres")]
        [MaxLength(40, ErrorMessage = "Este campo deve ter at� 40 caracteres")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Informe um email v�lido.")]
        public string email { get; set; }

        [Required(ErrorMessage = "Este campo � obrigat�rio.")]
        [MinLength(5, ErrorMessage = "Este campo deve ter mais de 5 caracteres")]
        [MaxLength(10, ErrorMessage = "Este campo deve ter at� 10 caracteres")]
        public string senha { get; set; }
    }
}