﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MazzaFC_API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    CdCategoria = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.CdCategoria);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CdCategoria = table.Column<int>(type: "int", nullable: false),
                    CategoriaCdCategoria = table.Column<int>(type: "int", nullable: true),
                    preco = table.Column<decimal>(type: "decimal(10,2)", precision: 10, scale: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Produtos_Categorias_CategoriaCdCategoria",
                        column: x => x.CategoriaCdCategoria,
                        principalTable: "Categorias",
                        principalColumn: "CdCategoria",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Categorias",
                columns: new[] { "CdCategoria", "Descricao" },
                values: new object[] { 1, "alimento" });

            migrationBuilder.InsertData(
                table: "Categorias",
                columns: new[] { "CdCategoria", "Descricao" },
                values: new object[] { 2, "bebida" });

            migrationBuilder.InsertData(
                table: "Categorias",
                columns: new[] { "CdCategoria", "Descricao" },
                values: new object[] { 3, "outros" });

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_CategoriaCdCategoria",
                table: "Produtos",
                column: "CategoriaCdCategoria");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Categorias");
        }
    }
}
