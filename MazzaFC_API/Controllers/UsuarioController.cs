using MazzaFC_API.Data;
using MazzaFC_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MazzaFC_API.Controllers
{
    [ApiController]
    [Route("v1/Usuario")]
    public class UsuarioController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public Usuario Login([FromServices] DataContext dataContext, [FromBody] Usuario usuario)
        {
            var user = dataContext.Usuarios.FirstOrDefault(x => x.email == usuario.email && x.senha == usuario.senha);

            return user;
        }

    }
}