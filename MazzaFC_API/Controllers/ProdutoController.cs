using MazzaFC_API.Data;
using MazzaFC_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MazzaFC_API.Controllers
{
    [ApiController]
    [Route("v1/Produtos")]
    public class ProdutoController : ControllerBase
    {

        [HttpGet]
        [Route("")]
        public List<Produto> Get([FromServices] DataContext dataContext)
        {
           // var produtos =  dataContext.Produtos.Include(x => x.Categoria).ToList();
            var produtos = dataContext.Produtos.ToList();

            return produtos;
        }

        [HttpGet]
        [Route("{id:int}")]
        public Produto GetId([FromServices] DataContext dataContext, int id)
        {
            var produto = dataContext.Produtos.FirstOrDefault(x => x.ID == id) ;
            return produto;
        }


        [HttpGet]
        [Route("verificaProduto/{nome}")]
        public Produto GetName([FromServices] DataContext dataContext, string nome)
        {
            var produtoNome = dataContext.Produtos.FirstOrDefault(x => x.Nome == nome);
            return produtoNome;
        }

        [HttpGet]
        [Route("categorias/{id:int}")]
        public List<Produto> GetCategoria([FromServices] DataContext dataContext, int id)
        {
            var produtos = dataContext.Produtos.Include(x => x.CdCategoria)
                .Where(x => x.CdCategoria == id)
                .ToList();

            return produtos;
        }

        [HttpPost]
        [Route("")]
        public ActionResult<Produto> Post([FromServices] DataContext dataContext, [FromBody]Produto produto)
        {
            if (ModelState.IsValid)
            {
  
                    dataContext.Produtos.Add(produto);
                    dataContext.SaveChanges();
                    return produto;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        [Route("{id:int}")]
        public ActionResult<Produto> Update([FromServices] DataContext dataContext, [FromBody] Produto produto, int id)
        {
            if (ModelState.IsValid)
            {
                Produto prod = dataContext.Produtos.Where(x => x.ID == id).FirstOrDefault();
                prod.CdCategoria = produto.CdCategoria;
                prod.Nome = produto.Nome;
                prod.preco = produto.preco;
                dataContext.Produtos.Update(prod);
                dataContext.SaveChanges();
                return prod;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete]
        [Route("delete/{id:int}")]
        public void Delete([FromServices] DataContext dataContext, int id)
        {
            Produto prod = dataContext.Produtos.Where(x => x.ID == id).FirstOrDefault();
            dataContext.Remove(prod);
            dataContext.SaveChanges();
        }
    }
}