

using MazzaFC_API.Data;
using MazzaFC_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MazzaFC_API.Controllers
{
    [ApiController]
    [Route("v1/categorias")]

    public class CategoriaController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Categoria>>> Get([FromServices] DataContext dataContext)
        {
            var categorias = await dataContext.Categorias.ToListAsync();
            return categorias;
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Categoria>> Post([FromServices] DataContext dataContext, [FromBody] Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                dataContext.Categorias.Add(categoria);
                await dataContext.SaveChangesAsync();
                return categoria;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}