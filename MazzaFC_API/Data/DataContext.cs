using MazzaFC_API.Models;
using Microsoft.EntityFrameworkCore;

namespace MazzaFC_API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            :base(options)
        {
        }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>()
                .HasData(
                new Categoria { CdCategoria = 1, Descricao = "alimento" },
                new Categoria { CdCategoria = 2, Descricao = "bebida" },
                new Categoria { CdCategoria = 3, Descricao = "outros" }
                );

            modelBuilder.Entity<Usuario>()
               .HasData(
               new Usuario {Id = 1 , email = "teste@teste.com", senha = "teste123" }
               );

            modelBuilder.Entity<Produto>()
                .Property(p => p.preco)
                .HasPrecision(10, 2);

            base.OnModelCreating(modelBuilder);
        }
    }
}