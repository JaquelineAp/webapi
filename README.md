# Exemplo de Desenvolvimento e utilização de Web API


## 🛠 Instalação
Tecnologias necessárias para utilização do projeto:
- Visual Studio 2019
- SQL Server

Realizar download do [**.Net Framework 5.0**](https://dotnet.microsoft.com/download/visual-studio-sdks) 

Realizar instalação do Asp.Net MVC

## 👨‍💻👩‍💻 Como Compilar

- Clone o projeto
- Abra o projeto através do Visual Studio 
- Acesse na barra superior, botão View >> Other Windows >> Package Manager Console
- Altere a string de conexão no arquivo MazzaFC_API/MazzaFC_API/appsettings.json, TAG DefaultConnection
- Altere a URI no arquivo MazzaFC_API/MazzaFC_API/ConsumindoAPI_Web/Helper/ProdutoHelper.cs
- Realize a instalação  no projeto MazzaFC dos seguintes pacotes através de Project >> Manage Nuget Package
  - Microsoft.EntityFrameworkCore {5.0.9}
  - Microsoft.EntityFrameworkCore.SqlServer {5.0.9}
  - Microsoft.EntityFrameworkCore.Tools {5.0.9}
  - Swashbuckle.AspNetCore {5.0.9}
- Realize a instalação  no projeto ConsumindoAPI_Web dos seguintes pacotes através de Project >> Manage Nuget Package
  - EntityFramework {6.2.0}  
  - Microsoft.AspNet.Identity.Core {2.2.3}     
  - System.Net.Http.Json  {5.0.0} 

- Execute o comando abaixo para criar o banco de dados: 
```bash
Update-database
```

## 👨‍💻👩‍💻 Utilizando a API 

Após criado o banco de dados, execute o projeto através do botão "Start"(F5)
 - Ao executar o projeto, serão abertas 2 abas no navegador
   - Uma aba referente a documentação do swagger, nele é possível visualizar os métodos da API e testa-los 
   - Uma aba referente ao projeto CRUD que irá consumir a API

Será aberta uma tela de Login 
 - O banco de dados já virá cadastrado com o usuário abaixo : 
  email: teste@teste.com
  senha: teste123

Para criar novos usuários, executar o comando abaixo no banco de dados:

```bash
INSERT INTO Usuarios VALUES ('informe o usuario','informe a senha')
```
