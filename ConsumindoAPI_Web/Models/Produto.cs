﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumindoAPI_Web.Models
{
    public class ProdutoViewModel
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public int CdCategoria { get; set; }
        // public Categoria Categoria { get; set; }
        public decimal preco { get; set; }
    }
}