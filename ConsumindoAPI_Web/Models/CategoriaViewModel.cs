﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumindoAPI_Web.Models
{
    public class CategoriaViewModel
    {
        public int CdCategoria { get; set; }
        public string Descricao { get; set; }
    }
}