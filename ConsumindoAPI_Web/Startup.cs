﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConsumindoAPI_Web.Startup))]
namespace ConsumindoAPI_Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
