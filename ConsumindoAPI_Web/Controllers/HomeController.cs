﻿using ConsumindoAPI_Web.Helper;
using ConsumindoAPI_Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ConsumindoAPI_Web.Controllers
{
    public class HomeController : Controller
    {
        ProdutoHelper produtoHelper = new ProdutoHelper();

        public async Task<ActionResult> Index()
        {
            if (Session["id"] != null)
            {
                List<ProdutoViewModel> produtos = new List<ProdutoViewModel>();
                HttpClient client = produtoHelper.Initial();
                HttpResponseMessage responseMessage = await client.GetAsync("v1/Produtos");

                if (responseMessage.IsSuccessStatusCode)
                {
                    var resultado = responseMessage.Content.ReadAsStringAsync().Result;
                    produtos = JsonConvert.DeserializeObject<List<ProdutoViewModel>>(resultado);
                }
                return View(produtos);
            }
            else
            {
                return RedirectToAction("Login");

            }
        }

        public ActionResult Login()
        {
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Usuarios usuario)
        {
            if (ModelState.IsValid)
            {
                using (UsuarioEntities user = new UsuarioEntities())
                {
                    var verificaUsuario = user.Usuarios
                        .Where(x => x.email.Equals(usuario.email) && x.senha.Equals(usuario.senha))
                        .FirstOrDefault();
                    if (verificaUsuario != null)
                    {
                        Session["id"] = verificaUsuario.Id.ToString();
                        Session["email"] = verificaUsuario.email.ToString();
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(usuario);
        }
        public async Task<ActionResult> Details(int id)
        {
            if (Session["id"] != null)
            {
                var produto = new ProdutoViewModel();
                HttpClient client = produtoHelper.Initial();
                HttpResponseMessage responseMessage = await client.GetAsync($"v1/Produtos/{id}");

                if (responseMessage.IsSuccessStatusCode)
                {
                    var resultado = responseMessage.Content.ReadAsStringAsync().Result;
                    produto = JsonConvert.DeserializeObject<ProdutoViewModel>(resultado);
                }
                return View(produto);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Edit(int? id)
        {
            if (Session["id"] != null)
            {
                ProdutoViewModel produto = new ProdutoViewModel();
                HttpClient client = produtoHelper.Initial();

                if (id != null)
                {
                    var responseMessage = client.GetAsync($"v1/Produtos/{id}");
                    responseMessage.Wait();
                    var retorno = responseMessage.Result;

                    if (retorno.IsSuccessStatusCode)
                    {
                        var produtoEditado = retorno.Content.ReadAsAsync<ProdutoViewModel>();
                        produtoEditado.Wait();
                        produto = produtoEditado.Result;
                        return View(produto);
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                return View(produto);
            }
            else
            {
                return RedirectToAction("Login");

            }

        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, ProdutoViewModel produto)
        {

            if (produto != null)
            {
                ProdutoViewModel produtoEditado = new ProdutoViewModel();
                HttpClient client = produtoHelper.Initial();
                HttpResponseMessage verificaSeExiste = await client.GetAsync($"v1/Produtos/verificaProduto/{produto.Nome}");

                if (verificaSeExiste.IsSuccessStatusCode)
                {
                    var resultado = verificaSeExiste.Content.ReadAsStringAsync().Result;
                    produtoEditado = JsonConvert.DeserializeObject<ProdutoViewModel>(resultado);
                    if (resultado == "" || produtoEditado.ID == id)
                    {
                        var put = client.PutAsJsonAsync<ProdutoViewModel>($"v1/Produtos/{id}", produto);
                        put.Wait();
                        var result = put.Result;

                        if (result.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Nome", "Já existe um produto cadastrado com este nome.");
                        return View();
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                return RedirectToAction("Index");
            }

            return View();

        }
        public ActionResult create()
        {
            if (Session["id"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public async Task<ActionResult> create(ProdutoViewModel produto)
        {

            HttpClient client = produtoHelper.Initial();
            HttpResponseMessage verificaSeExiste = await client.GetAsync($"v1/Produtos/verificaProduto/{produto.Nome}");

            if (verificaSeExiste.IsSuccessStatusCode)
            {
                var resultado = verificaSeExiste.Content.ReadAsStringAsync().Result;
                if (resultado == "")
                {
                    var post = client.PostAsJsonAsync<ProdutoViewModel>("v1/Produtos/", produto);
                    post.Wait();
                    var result = post.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("Nome", "Produto já cadastrado");
                }
            }
            return View();
        }

        public ActionResult delete(int id)
        {
            if (Session["id"] != null)
            {
                HttpClient client = produtoHelper.Initial();
                var produtodeletado = client.DeleteAsync($"v1/Produtos/delete/{id}");
                produtodeletado.Wait();

                var result = produtodeletado.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("Login");
        }
    }
}